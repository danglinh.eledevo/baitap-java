package com.example.baitapjava.bai1;

public class bai1 {

  public static void main(String[] args) {
    // hinh vuong
    HinhVuong hinhVuong = new HinhVuong(5);
    hinhVuong.perimeter();
    hinhVuong.area();
    // hinh chu nhat
    HinhChuNhat hinhChuNhat = new HinhChuNhat(5, 3);
    hinhChuNhat.perimeter();
    hinhChuNhat.area();
    // hinh tron
    HinhTron hinhTron = new HinhTron(5);
    hinhTron.perimeter();
    hinhTron.area();
    // hinh thang
    HinhThang hinhThang = new HinhThang(4, 6, 2, 4, 5);
    hinhThang.perimeter();
    hinhThang.area();
  }

  public static abstract class Shape {

    public String name;

    abstract void perimeter(); // chu vi

    abstract void area(); // dien tich


  }

  public static class HinhVuong extends Shape {

    private int canh;

    public HinhVuong(int canh) {
      this.canh = canh;
    }

    @Override
    void perimeter() {
      int chuvihinhvuong = canh + canh + canh + canh;
      System.out.println("Chu vi hinh vuong la :" + chuvihinhvuong);
    }

    @Override
    void area() {
      int dientichhinhvuong = canh * canh;
      System.out.println("dien tich hinh vuong la :" + dientichhinhvuong);
    }
  }

  public static class HinhChuNhat extends Shape {

    int rong;
    int dai;

    public HinhChuNhat(int rong, int dai) {
      this.rong = rong;
      this.dai = dai;
    }

    @Override
    void perimeter() {
      int chuvihinhchunhat = (rong + dai) * 2;
      System.out.println("Chu vi hinh chu nhat la :" + chuvihinhchunhat);
    }

    @Override
    void area() {
      int dientichhinhchunhat = rong * dai;
      System.out.println("dien tich hinh chu nhat la :" + dientichhinhchunhat);
    }
  }

  public static class HinhTron extends Shape {

    int banKinh;

    final double soPi = Math.PI;

    public HinhTron(int banKinh) {
      this.banKinh = banKinh;

    }

    @Override
    void perimeter() {
      int chuvihinhtron = (int) (banKinh * 2 * soPi);
      System.out.println("Chu vi hinh tron la :" + chuvihinhtron);
    }

    @Override
    void area() {
      int dientichhinhtron = (int) ((banKinh * banKinh) * soPi);
      System.out.println("dien tich hinh tron la :" + dientichhinhtron);
    }
  }

  public static class HinhThang extends Shape {


    int dayTren;
    int dayDuoi;
    int canhTrai;
    int canhPhai;
    int chieuCao;

    public HinhThang(int dayTren, int dayDuoi, int canhTrai, int canhPhai, int chieuCao) {
      this.dayTren = dayTren;
      this.dayDuoi = dayDuoi;
      this.canhTrai = canhTrai;
      this.canhPhai = canhPhai;
      this.chieuCao = chieuCao;
    }

    @Override
    void perimeter() {
      int chuvihinhthang = dayTren + dayDuoi + canhTrai + canhPhai;
      System.out.println("Chu vi hinh thang la :" + chuvihinhthang);
    }

    @Override
    void area() {
      int dientichhinhthang = chieuCao * ((dayTren + dayDuoi) / 2);
      System.out.println("dien tich hinh thang la :" + dientichhinhthang);
    }
  }
}


