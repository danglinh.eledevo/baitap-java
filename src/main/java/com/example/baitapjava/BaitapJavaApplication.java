package com.example.baitapjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaitapJavaApplication {

  public static void main(String[] args) {
    SpringApplication.run(BaitapJavaApplication.class, args);
  }

}
